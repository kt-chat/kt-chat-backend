<?php

$method = $_SERVER['REQUEST_METHOD'];
require_once "helper_functions.php";
require_once "auth.php";

$user_id = $_SESSION['user_id'];

if($method === 'GET')
{
	require_once "db_connect.php";

	$sql = "SELECT u.user, m.datetime, m.message FROM users AS u, messages AS m WHERE u.id=m.user_id";
		
	query_response($connection, $sql);
	
	if(isset($errors)) echo json_encode($errors);
	
	// close connection
	$connection->close();
}
elseif($method === 'POST')
{

	$data = get_data();
	
	if(empty($data['message']))
	{
		$errors['message'] = return_error(403, "Message field can not be empty");
		echo json_encode($errors);
	}
	else
	{
		$message = $data['message'];
		require_once "db_connect.php";
		
		if($connection->query("INSERT INTO messages VALUES (NULL, $user_id, '$message', now())"))
		{
			$sql = "SELECT users.user, messages.datetime, messages.message FROM users, messages
			WHERE user_id=$user_id AND users.id = messages.user_id ORDER BY messages.datetime DESC LIMIT 1";
						
			query_response($connection, $sql);
			$connection->close();
		}
	}
}
else
{
	$errors['info'] = return_error(405, "Method not allowed :(");
	echo json_encode($errors);
}