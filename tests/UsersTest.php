<?php

require('vendor/autoload.php');

//absolute path

class UsersTest extends PHPUnit_Framework_TestCase
{
    protected $client;
    protected $jar;
    protected $connection;
    protected $BASE_URL = 'http://localhost/Chat/';

    function __construct() {
        require('db_credentials.php');
        require('db_connect.php');
        $this->connection = $connection;
    }
    
    function __destruct() {
        $this->connection->close();
    }
    
    protected function setUp()
    {
        $this->client = new GuzzleHttp\Client(['http_errors' => false]);
        $this->jar = new \GuzzleHttp\Cookie\CookieJar();
        $this->client->request('POST', $this->BASE_URL.'login', [
            GuzzleHttp\RequestOptions::JSON => [
                'user' => 'Adam',
                'pass' => 'qwerty',
            ],
            'cookies' => $this->jar,
        ]);
    }
    
    public function test_GET_users_with_permission()
    {        
        $response = $this->client->request('GET', $this->BASE_URL.'users/1',[
            'cookies' => $this->jar,
        ]);
        $this->assertEquals(200, $response->getStatusCode());
       
        $data = json_decode($response->getBody(), true)[0];

        $this->assertEquals(3, count($data));
        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('user', $data);
        $this->assertArrayHasKey('email', $data);
        $this->assertArrayNotHasKey('pass', $data);
    }
    
    public function test_GET_message_with_permission()
    {
        $response = $this->client->request('GET', $this->BASE_URL.'message',[
            'cookies' => $this->jar,
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        
        $data = json_decode($response->getBody(), true)[0];
        
        $this->assertEquals(3, count($data));
        $this->assertArrayHasKey('user', $data);
        $this->assertArrayHasKey('datetime', $data);
        $this->assertArrayHasKey('message', $data);
    }
    public function test_POST_message_with_permission()
    { 
        $datetime = new DateTime();

        $response = $this->client->request('POST', $this->BASE_URL.'message',[
            GuzzleHttp\RequestOptions::JSON => [
                'message' => 'Well done!',
            ],
            'cookies' => $this->jar,
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        
        $data = json_decode($response->getBody(), true)[0];
        $this->assertArrayHasKey('user', $data);
        $this->assertArrayHasKey('datetime', $data);
        $this->assertArrayHasKey('message', $data); 
        
        $time = DateTime::createFromFormat('Y-m-d H:i:s', $data['datetime']);
        $diff = $datetime->diff($time);
        $diff = (int)$diff->format('%s');
        $this->assertLessThanOrEqual(1, $diff);
    }
    
    public function test_POST_message_noData()
    {
        $response = $this->client->request('POST', $this->BASE_URL.'message',[
            GuzzleHttp\RequestOptions::JSON => [
                'message' => '',
            ],
            'cookies' => $this->jar,
        ]);

        $this->assertEquals(403, $response->getStatusCode());
    }
    
    public function test_POST_users()
    {  
       $this->connection->query("DELETE FROM users WHERE user = 'qwerty123'");
       $response = $this->client->request('POST', $this->BASE_URL.'users', [
            GuzzleHttp\RequestOptions::JSON => [
                'user' => 'qwerty123',
                'pass' => 'qwerty123',
                'email' => 'qwerty@gmail.com',
            ],
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $result = $this->connection->query("SELECT * FROM users WHERE user='qwerty123'");
        $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $this->assertEquals(1, $result->num_rows);
        $result->free_result(); 
        $this->connection->query("DELETE FROM users WHERE user = 'qwerty123'");       
    }
    public function test_POST_users_noData()
    {
        $response = $this->client->request('POST', $this->BASE_URL.'users', [
           GuzzleHttp\RequestOptions::JSON => [
               'user' => '',
               'pass' => '',
               'email' => '',
           ],
       ]);
       $this->assertEquals(406, $response->getStatusCode());
    }
    public function test_POST_noUser()
    {
        $response = $this->client->request('POST', $this->BASE_URL.'users', [
            GuzzleHttp\RequestOptions::JSON => [
                'user' => '',
                'pass' => 'qwerty123',
                'email' => 'qwerty@gmail.com',
            ],
        ]);
        $this->assertEquals(406, $response->getStatusCode());
    }
    public function test_POST_noPass()
    {
        $response = $this->client->request('POST', $this->BASE_URL.'users', [
            GuzzleHttp\RequestOptions::JSON => [
                'user' => 'qwerty123',
                'pass' => '',
                'email' => 'qwerty@gmail.com',
            ],
        ]);
        $this->assertEquals(406, $response->getStatusCode());
    }
    public function test_POST_noEmail()
    {
        $response = $this->client->request('POST', $this->BASE_URL.'users', [
            GuzzleHttp\RequestOptions::JSON => [
                'user' => 'qwerty123',
                'pass' => 'qwerty123',
                'email' => '',
            ],
        ]);
        $this->assertEquals(406, $response->getStatusCode());
    }
    
    public function test_POST_UserAlnum()
    {
        $response = $this->client->request('POST', $this->BASE_URL.'users', [
            GuzzleHttp\RequestOptions::JSON => [
                'user' => '���',
                'pass' => 'qwerty123',
                'email' => 'qwerty@gmail.com',
            ],
        ]);
        $this->assertEquals(406, $response->getStatusCode());
    }
    public function test_POST_UserShort()
    {
        $response = $this->client->request('POST', $this->BASE_URL.'users', [
            GuzzleHttp\RequestOptions::JSON => [
                'user' => 'q',
                'pass' => 'qwerty123',
                'email' => 'qwerty@gmail.com',
            ],
        ]);
        $this->assertEquals(406, $response->getStatusCode());
    }
    public function test_POST_UserLong()
    {
        $response = $this->client->request('POST', $this->BASE_URL.'users', [
            GuzzleHttp\RequestOptions::JSON => [
                'user' => 'qwerty123123123123123123123123',
                'pass' => 'qwerty123',
                'email' => 'qwerty@gmail.com',
            ],
        ]);
        $this->assertEquals(406, $response->getStatusCode());
    }
    public function test_POST_PassShort()
    {
        $response = $this->client->request('POST', $this->BASE_URL.'users', [
            GuzzleHttp\RequestOptions::JSON => [
                'user' => 'qwerty123',
                'pass' => 'q',
                'email' => 'qwerty@gmail.com',
            ],
        ]);
        $this->assertEquals(406, $response->getStatusCode());
    }
    public function test_POST_PassLong()
    {
        $response = $this->client->request('POST', $this->BASE_URL.'users', [
            GuzzleHttp\RequestOptions::JSON => [
                'user' => 'qwerty123',
                'pass' => 'qwerty123123123123123123123123',
                'email' => 'qwerty@gmail.com',
            ],
        ]);
        $this->assertEquals(406, $response->getStatusCode());
    }
    public function test_POST_EmailValid()
    {
        $response = $this->client->request('POST', $this->BASE_URL.'users', [
            GuzzleHttp\RequestOptions::JSON => [
                'user' => 'qwerty123',
                'pass' => 'qwerty123',
                'email' => 'qwerty',
            ],
        ]);
        $this->assertEquals(406, $response->getStatusCode());
    }
    public function test_POST_UserExists()
    {
        $pass_hash = password_hash('qwerty123', PASSWORD_DEFAULT);
        $this->connection->query("INSERT INTO users VALUES (null, 'qwerty123', '$pass_hash', 'qwerty@gmail.com')");
        
        $result = $this->connection->query("SELECT * FROM users WHERE user='qwerty123'");
        $this->assertEquals(1, $result->num_rows);
  
        $response = $this->client->request('POST', $this->BASE_URL.'users', [
            GuzzleHttp\RequestOptions::JSON => [
                'user' => 'qwerty123',
                'pass' => 'qwerty123',
                'email' => 'qwerty@gmail.com',
            ],
        ]);
        $this->assertEquals(406, $response->getStatusCode());
        
        $this->connection->query("DELETE FROM users WHERE user='qwerty123'");
        $result = $this->connection->query("SELECT * FROM users WHERE user='qwerty123'");
        $this->assertEquals(0, $result->num_rows);
    }

    public function test_GET_logout()
    {
        $response = $this->client->request('GET', $this->BASE_URL.'logout');
        $this->assertEquals(200, $response->getStatusCode());
    }
    public function test_DELETE_users_with_permission()
    {  
       $jar = new \GuzzleHttp\Cookie\CookieJar();
       
       // 

       $pass_hash = password_hash('qwerty123', PASSWORD_DEFAULT);
       $this->connection->query("INSERT INTO users VALUES (null, 'qwerty123', '$pass_hash', 'qwerty@gmail.com')");
       
       $result = $this->connection->query("SELECT * FROM users WHERE user='qwerty123'");
       $this->assertEquals(1, $result->num_rows);
       $row = $result->fetch_assoc();

       $id = $row['id'];
      
       $this->client->request('POST', $this->BASE_URL.'login', [
           GuzzleHttp\RequestOptions::JSON => [
               'user' => 'qwerty123',
               'pass' => 'qwerty123',
           ],
           'cookies' => $jar,
       ]);

       $response = $this->client->request('DELETE', $this->BASE_URL.'users/'.$id,[
           'cookies' => $jar,
        ]);
       $result = $this->connection->query("SELECT id FROM users WHERE user='qwerty123'");
       $this->assertEquals(0, $result->num_rows);
       $this->assertEquals(200, $response->getStatusCode());
    } 
}