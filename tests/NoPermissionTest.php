<?php

require('vendor/autoload.php');

class NoPermissionTest extends PHPUnit_Framework_TestCase
{
    protected $client;
    protected $BASE_URL = 'http://localhost/Chat/';

    protected function setUp()
    {
        $this->client = new GuzzleHttp\Client(['http_errors' => false]);
    }
    
    public function test_GET_user_no_permission()
    {     
        $response = $this->client->request('GET', $this->BASE_URL.'users/1');
        $this->assertEquals(403, $response->getStatusCode());
    }
    public function test_GET_users_no_permission()
    {
        $response = $this->client->request('GET', $this->BASE_URL.'users');
        $this->assertEquals(403, $response->getStatusCode());
    }
    public function test_GET_message_no_permission()
    {
        $response = $this->client->request('GET', $this->BASE_URL.'message');
        $this->assertEquals(403, $response->getStatusCode());
    }  
    
    public function test_DELETE_users_no_permission()
    {
        $response = $this->client->request('DELETE', $this->BASE_URL.'users/1');
        $this->assertEquals(403, $response->getStatusCode());
    }
}