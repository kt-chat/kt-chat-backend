<?php

function return_error($response_code, $error_info)
{
	if(!$response_code) $response_code = 406;
		
	header("Content-Type:application/json", true, $response_code);	
	return $info['error'] = $error_info;
}

function get_data()
{
	$data = json_decode(file_get_contents('php://input'), true);
	foreach($data as $key => $value) $data[$key] = htmlentities($value, ENT_QUOTES, "UTF-8");
	return $data;
}

function check_length($var, $min, $max)
{
	if(strlen($var)<$min || strlen($var) >$max) 
		return false;
	
	return true;
}

function query_response($connection, $sql)
{
	try
	{
		$result = $connection->query($sql);
		
		if(!$result) throw new exception($connection->error);
		
		$rows = [];
		while($row = $result->fetch_assoc())
		{
			$rows[] = $row;
		}

		header("Content-Type:application/json", true, 200);
		echo json_encode($rows);
		$result->free_result();
	}
	catch(Exception $e)
	{
		$errors['info'] = return_error(500, $e->getMessage());
		echo json_encode($errors);
	}
}
function user_pass_empty($data)
{
	if(empty($data['user']))
	{
		$errors['user'] = return_error(null, "User field can not be empty");
	}
	if(empty($data['pass']))
	{
		$errors['pass'] = return_error(null, "Password field can not be empty");
	}
	if(isset($errors)) return $errors;
	else return null;
}

function errorHandler($errno, $errstr, $errfile, $errline)
{
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}