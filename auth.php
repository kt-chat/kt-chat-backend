<?php

session_start();

if(!isset($_SESSION['user_id']))
{
	require_once "helper_functions.php";
	$errors['info'] = return_error(403, "You have not permissions for this site");
	echo json_encode($errors);
	exit();
}