<?php

$method = $_SERVER['REQUEST_METHOD'];
require_once "helper_functions.php";

if($method === 'POST')
{
	session_start();
			
	$data = get_data();
	$errors = user_pass_empty($data);

	if(!empty($errors))
	{
		echo json_encode($errors);
		exit();
	}
			
	$user = $data['user'];
	$password = $data['pass'];
			
	require_once "db_connect.php";
			
	if($result = $connection->query(
		sprintf("SELECT * FROM users WHERE user='%s'",
		mysqli_real_escape_string($connection, $user))))
	{
		if($result->num_rows>0)
		{
			$row = $result->fetch_assoc();
				
			if(password_verify($password, $row['pass']))
			{
				$_SESSION['user_id'] = $row['id'];
				$result->free_result();
				header('Location: users.php');
			}
			else
			{
				$errors['info'] = return_error(401, "Incorrect password!");
			}

		} else 
		{				
			$errors['info'] = return_error(401, "Incorrect login or password!");
		}
	}
	else
	{
		$errors['info'] = return_error(500, "Internal Server Error");	
	}
		
	if(isset($errors)) echo json_encode($errors);
		
	$connection->close();
}
else 
{
	$errors['info'] = return_error(405, "Method not allowed :(");
	echo json_encode($errors);
}