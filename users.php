<?php

$method = $_SERVER['REQUEST_METHOD'];

require_once "helper_functions.php";

if($method === 'GET')
{
	require_once "auth.php";
	
	if(isset($_GET['id']))
	{
		if($_GET['id'] === $_SESSION['user_id'])
		{
			$sql = "SELECT id, user, email FROM users WHERE id = '{$_SESSION['user_id']}'";
		}
		else
		{
			$errors['info'] = return_error(403, "You have not permissions for this site");
		}
	}
	elseif(isset($_SESSION['user_id']))
	{
		$sql = "SELECT id, user, email FROM users WHERE id = '{$_SESSION['user_id']}'";
	}
	else
	{
		$errors['info'] = return_error(403, "You have not permissions for this site");
	}
	
	if(isset($errors))
	{
		echo json_encode($errors);
		exit();
	}
	
	require_once "db_connect.php";
	
	query_response($connection, $sql);
	
	//close connection
	$connection->close();
	
} elseif ($method === 'POST')
{
	$data = get_data();
	$errors = user_pass_empty($data);
		
	if(empty($data['email']))
	{
		$errors['email'] = return_error(null, "Email field can not be empty");
	}

	if(!empty($errors))
	{
		echo json_encode($errors);
		exit();
	}
	
	$user = $data['user'];
	$pass = $data['pass'];
	$email = $data['email'];

	//Check if user is correct
	if(!check_length($user, 3, 15))
	{
		$errors['user'] = return_error(null, "Nickname needs to have between 3 and 15 characters");
	}
			
	if(ctype_alnum($user)==false)
	{
		$errors['user'] = return_error(null, "Nickname needs to consist of alphanumeric characters!");
	}
		
	//Check if email is correct
	$emailB = filter_var($email, FILTER_SANITIZE_EMAIL);
	if(filter_var($emailB, FILTER_VALIDATE_EMAIL)==false || $emailB!=$email)
	{
		$errors['email'] = return_error(null, "Incorrect email!");
	}
		
	//Check if password is correct
	if(!check_length($pass, 8, 20))
	{
		$errors['pass'] = return_error(null, "Password needs to have between 8 and 20 characters");
	}
	else $pass_hash = password_hash($pass, PASSWORD_DEFAULT);
		
	require_once "db_connect.php";
		
	//Check if user already exists
	$sql = "SELECT * FROM users WHERE user = '$user'";
		
	try
	{
		$result = $connection->query($sql);
			
		if(!$result) throw new exception($connection->error);
		else
		{			
			if($result->num_rows>0)
			{
				$errors['user'] = return_error(null, "Given nickname already exists in the database! Choose another one.");
			}
		}
	}
	catch(Exception $e)
	{
		$errors['info'] = return_error(500, "Server error!");
	}
		
	if(isset($errors))	
	{
		echo json_encode($errors);
	}
	else
	{	
		//query	
		$sql = "INSERT INTO users VALUES (NULL, '$user', '$pass_hash','$email')";
		
		try
		{
			if($connection->query($sql))
			{
				header("Content-Type:application/json", true, 200);
				$info['info'] = "Thank you for registration in our service! Now you can sign in into your account!";
				echo json_encode($info);
			}
			else
			{
				throw new exception($connection->error); 
			}
		}
		catch(Exception $e)
		{
			$errors['info'] = return_error(500, "Server error");
			echo json_encode($errors);
		}
	}
	// close connection
    $connection->close();	
} elseif ($method === 'DELETE')
{
    require_once "auth.php";
    echo $_SESSION['user_id'];
    if(isset($_GET['id']) && $_GET['id'] == $_SESSION['user_id'])
    {
        require_once "db_connect.php";
        $sql = "DELETE FROM users WHERE id = '{$_SESSION['user_id']}'";

        $result = $connection->query($sql);
        
        if(!$result)
        {
            $errors['info'] = return_error(500, "Database Error");
            echo json_encode($errors);
        }
        else
        {
            header("Content-Type:application/json", true, 200);
            $info['info'] = "Your account was deleted!";
            echo json_encode($info);
        }
    }
    else
    {
        $errors['info'] = return_error(403, "You have not permissions for this site");
        echo json_encode($errors);
    }
}
else 
{
	$errors['info'] = return_error(405, "Method not allowed :(");
	echo json_encode($errors);
}