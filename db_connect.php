<?php

require_once("db_credentials.php");

require_once "helper_functions.php";

set_error_handler("errorHandler");

try
{
	$connection = new mysqli($host, $db_user, $db_password, $db_name);
}
catch(ErrorException $e)
{
	$errors['info'] = return_error(500, $e->getMessage());
	echo json_encode($errors);
	exit();
}