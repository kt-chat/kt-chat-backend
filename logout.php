<?php

require_once "helper_functions.php";

$method = $_SERVER['REQUEST_METHOD'];

if($method === 'GET')
{
	session_start();
	session_unset();

	header("Content-Type:application/json", true, 200);
	$result['info'] = "You have been successfully logged out!";
	echo json_encode($result);
}
else
{
	$errors['info'] = return_error(405, "Method not allowed :(");
	echo json_encode($errors);
}